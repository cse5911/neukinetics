﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Threading;

// added in by Sonny (messing around)
using Microsoft.Kinect;
using System.Windows.Forms;
using System.IO;
using System.Media;


namespace NeuKinetics
{
    /// <summary>
    /// Interaction logic for Welcome.xaml
    /// </summary>

    
    public partial class Welcome : Window
    {

        static string[] art = new string[3];

        #region Member Variables
        private KinectSensor _Kinect;
        #endregion Member Variables
        private DispatcherTimer timerImageChange;
        private Image[] ImageControls;
        private List<ImageSource> Images = new List<ImageSource>();
        private static string[] ValidImageExtensions = new[] { ".png", ".jpg", ".jpeg", ".bmp", ".gif" };
        private static string[] TransitionEffects = new[] { "Fade" };
        private string TransitionType, strImagePath = "";
        private int CurrentSourceIndex, CurrentCtrlIndex, EffectIndex = 0, IntervalTimer = 1;
        private DispatcherTimer timer;

        public Welcome()
        {

            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Start();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;

            clock_textbox.Visibility = System.Windows.Visibility.Visible;

            // Initialize Image control, Image directory path and Image timer.
            string root_dir = AppDomain.CurrentDomain.BaseDirectory;
            IntervalTimer = Convert.ToInt32(5);
            strImagePath = root_dir+"Images\\SlideShowImages";
            ImageControls = new[] { myImage, myImage2 };

            LoadImageFolder(strImagePath);

            timerImageChange = new DispatcherTimer();
            timerImageChange.Interval = new TimeSpan(0, 0, IntervalTimer);
            timerImageChange.Tick += new EventHandler(timerImageChange_Tick);

            // Sonny messing around
            this.Loaded += (s, e) => { DiscoverKinectSensor(); };
            this.Unloaded += (s, e) => { this.Kinect = null; };
        }


        public void timer_Tick(object sender, EventArgs e)
        {
            clock_textbox.Text = DateTime.Now.ToLongTimeString();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            about_viewbox.Visibility = System.Windows.Visibility.Hidden;
            PlaySlideShow();
            timerImageChange.IsEnabled = true;
        }


        private void LoadImageFolder(string folder)
        {
            ErrorText.Visibility = Visibility.Collapsed;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            if (!System.IO.Path.IsPathRooted(folder))
                folder = System.IO.Path.Combine(Environment.CurrentDirectory, folder);
            if (!System.IO.Directory.Exists(folder))
            {
                ErrorText.Text = "The specified folder does not exist: " + Environment.NewLine + folder;
                ErrorText.Visibility = Visibility.Visible;
                return;
            }
            Random r = new Random();
            var sources = from file in new System.IO.DirectoryInfo(folder).GetFiles().AsParallel()
                          where ValidImageExtensions.Contains(file.Extension, StringComparer.InvariantCultureIgnoreCase)
                          orderby r.Next()
                          select CreateImageSource(file.FullName, true);
            Images.Clear();
            Images.AddRange(sources);
            sw.Stop();
            Console.WriteLine("Total time to load {0} images: {1}ms", Images.Count, sw.ElapsedMilliseconds);
        }

        private ImageSource CreateImageSource(string file, bool forcePreLoad)
        {
            if (forcePreLoad)
            {
                var src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(file, UriKind.Absolute);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();
                src.Freeze();
                return src;
            }
            else
            {
                var src = new BitmapImage(new Uri(file, UriKind.Absolute));
                src.Freeze();
                return src;
            }
        }



        // SLIDE SHOW

        private void timerImageChange_Tick(object sender, EventArgs e)
        {
            PlaySlideShow();
        }

        private void PlaySlideShow()
        {
            try
            {
                if (Images.Count == 0)
                    return;

                var oldCtrlIndex = CurrentCtrlIndex;
                CurrentCtrlIndex = (CurrentCtrlIndex + 1) % 2;
                CurrentSourceIndex = (CurrentSourceIndex + 1) % Images.Count;

                Image imgFadeOut = ImageControls[oldCtrlIndex];
                Image imgFadeIn = ImageControls[CurrentCtrlIndex];
                ImageSource newSource = Images[CurrentSourceIndex];
                imgFadeIn.Source = newSource;

                TransitionType = TransitionEffects[EffectIndex].ToString();

                Storyboard StboardFadeOut = (Resources[string.Format("{0}Out", TransitionType.ToString())] as Storyboard).Clone();
                StboardFadeOut.Begin(imgFadeOut);
                Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString())] as Storyboard;
                StboardFadeIn.Begin(imgFadeIn);
            }

            catch (Exception ex) 
            {
                System.Diagnostics.Debug.Print(ex.Message);
            }

        }

        #region Methods

        // Sonny's Kinect statuses and pop up messages

        private void DiscoverKinectSensor()
        {
            KinectSensor.KinectSensors.StatusChanged += KinectSensors_StatusChanged;
            this.Kinect = KinectSensor.KinectSensors.FirstOrDefault(x => x.Status == KinectStatus.Connected);
        }

        private void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            switch (e.Status)
            {

                // Status: Connected

                case KinectStatus.Connected:
                    if (this.Kinect == null)
                    {
                        this.Kinect = e.Sensor;
                    }
                    break;

                // Status: Disconnected

                case KinectStatus.Disconnected:
                    if (this.Kinect == e.Sensor)
                    {
                        this.Kinect = null;
                        this.Kinect = KinectSensor.KinectSensors.FirstOrDefault(x => x.Status == KinectStatus.Connected);

                        if (this.Kinect == null)
                        {
                            //Notify the user that the sensor is disconnected
                            System.Windows.Forms.MessageBox.Show("The USB connection with the device has been broken.");
                            Console.WriteLine("Status: Disconnected");
                        }

                    }
                    break;

                // Status: NotPowered

                case KinectStatus.NotPowered:
                    if (this.Kinect == e.Sensor)
                    {
                        this.Kinect = null;
                        this.Kinect = KinectSensor.KinectSensors.FirstOrDefault(x => x.Status == KinectStatus.NotPowered);

                        if (this.Kinect == null)
                        {
                            //Notify the user that the sensor is disconnected
                            System.Windows.Forms.MessageBox.Show("The Kinect is not fully powered. The power provided by a USB connection is not sufficient to power the Kinect hardware. An additional power adapter is required.");
                            Console.WriteLine("Status: NotPowered");
                        }
                    }
                    break;

                //Handel all other statuses according to needs
            }
        }
        #endregion Methods





        #region Properties
        public KinectSensor Kinect
        {
            get { return this._Kinect; }

            set
            {
                if (this._Kinect != value)
                {
                    if (this._Kinect != null)
                    {
                        // Uninitialize
                        // UninitializeKinectSensor(this._Kinect);
                        this._Kinect = null;
                    }

                    if (value != null && value.Status == KinectStatus.Connected)
                    {
                        this._Kinect = value;
                        // Initialize
                        // InitializeKinectSensor(this._Kinect);
                    }
                }
            }
        }

        #endregion Properties





        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        private void demo1_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            Window main = new Demo1("demo1");
            main.Show();
            SystemSounds.Asterisk.Play();
           
        }

        private void demo2_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            Window main = new Demo1("demo2");
            main.Show();
            SystemSounds.Asterisk.Play();

        }

        private void about_btn_Click(object sender, RoutedEventArgs e)
        {
            if (about_viewbox.Visibility != System.Windows.Visibility.Hidden)
            {
                SystemSounds.Asterisk.Play();
                about_viewbox.Visibility = System.Windows.Visibility.Hidden;
                slide_show.Visibility = System.Windows.Visibility.Visible;
            }
            else 
            {
                SystemSounds.Asterisk.Play();
                slide_show.Visibility = System.Windows.Visibility.Hidden;
                about_viewbox.Visibility = System.Windows.Visibility.Visible;
            }
            
        }

        private void about_textbox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

    }
}
