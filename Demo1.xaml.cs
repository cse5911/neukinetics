﻿

namespace NeuKinetics
{
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using Microsoft.Kinect;
    using System;
    using System.Windows.Threading;
    using Microsoft.Win32;
    using System.Media;

    using System.Windows.Media.Imaging;
    using System.Windows.Interop;
    using System.Collections;
    using System.Collections.Generic;
    using System.Net.Mail;
    using System.Text;


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Demo1 : Window
    {
        // REP_INFO: Package encapsulating all info tracked about any
        public struct Rep_Info
        {
            public int number;                 // rep number within set
            public int performance;            // enumerated rep performance
            public int delta_time;                // duration of rep
        }
        // REPS: This array anticipates and stores the data for each rep
        public static int REP_COUNT = 10;       // number of reps to track
        int num_count = 5;
       // public Rep_Info[] reps = new Rep_Info[REP_COUNT];
        private List<Rep_Info> reps = new List<Rep_Info>();
        // PERFORMANCE ENUMERATION
        public static int AVERAGE = 0;
        public static int GOOD = 1;
        public static int EXCELLENT = 2;
        public string [] performance_labels = new string[3];

        // Clock member variables 
        private DispatcherTimer timer;
        private DispatcherTimer countdown;
        private DispatcherTimer rep_timer;
        private int rep_counter;
        private int counter = 5;

        private Boolean done_flag = true;
        private Boolean time_flag = false;
        private Boolean flag = false;
        private Boolean start_exercise_flag = false;

        private List<string> colorList = new List<string>();

        /// <summary>
        /// Width of output drawing
        /// </summary>
        private const float RenderWidth = 640.0f;

        /// <summary>
        /// Height of our output drawing
        /// </summary>
        private const float RenderHeight = 480.0f;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of body center ellipse
        /// </summary>
        private const double BodyCenterThickness = 10;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Brush used to draw skeleton center point
        /// </summary>
        private readonly Brush centerPointBrush = Brushes.Blue;

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor sensor;

        /// <summary>
        /// Drawing group for skeleton rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        // This will hold the value of the name of the exercise video
        private string video;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public Demo1(string video)
        {
            this.video = video;
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Start();
            timer.Interval = TimeSpan.FromSeconds(1);
            
            timer.Tick += timer_Tick;

            clock_textbox.Visibility = System.Windows.Visibility.Visible;
            scorecard_viewbox.Visibility = System.Windows.Visibility.Hidden;

            performance_labels[0] = "FAIR";
            performance_labels[1] = "GOOD";
            performance_labels[2] = "EXCELLENT";

        }

        public void timer_Tick(object sender, EventArgs e)
        {
            clock_textbox.Text = DateTime.Now.ToLongTimeString();
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping skeleton data
        /// </summary>
        /// <param name="skeleton">skeleton to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, RenderHeight - ClipBoundsThickness, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, RenderHeight));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(RenderWidth - ClipBoundsThickness, 0, ClipBoundsThickness, RenderHeight));
            }
        }


        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            start_exercise_btn.Visibility = System.Windows.Visibility.Visible;
            instructions.Visibility = System.Windows.Visibility.Visible;

            media_top.Play();
            media_bottom.Play();

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            // Display the drawing using our image control
            Image.Source = this.imageSource;

            // Look through all sensors and start the first connected one.
            // This requires that a Kinect is connected at the time of app startup.
            // To make your app robust against plug/unplug, 
            // it is recommended to use KinectSensorChooser provided in Microsoft.Kinect.Toolkit (See components in Toolkit Browser).
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (null != this.sensor)
            {
                // Turn on the skeleton stream to receive skeleton frames
                this.sensor.SkeletonStream.Enable();

                // Add an event handler to be called whenever there is new color frame data
                this.sensor.SkeletonFrameReady += this.SensorSkeletonFrameReady;

                // Start the sensor!
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (null != this.sensor)
            {
                this.sensor.Stop();
            }
        }

        /// <summary>
        /// Event handler for Kinect sensor's SkeletonFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            Skeleton[] skeletons = new Skeleton[0];

            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                // Draw a transparent background to set the render size
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                if (skeletons.Length != 0)
                {
                    foreach (Skeleton skel in skeletons)
                    {
                        RenderClippedEdges(skel, dc);

                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                           this.DrawBonesAndJoints(skel, dc);
                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(
                            this.centerPointBrush,
                            null,
                            this.SkeletonPointToScreen(skel.Position),
                            BodyCenterThickness,
                            BodyCenterThickness);
                        }
                    }
                }

                // prevent drawing outside of our render area
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));
            }
        }

        /// <summary>
        /// Draws a skeleton's bones and joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {
           
              Pen changeCol = SitDownExercise(skeleton);
           
            // Render Torso
            this.DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight, changeCol);

            // Left Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft, changeCol);

            // Right Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight, changeCol);

            // Left Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft, changeCol);

            // Right Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight, changeCol);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight, changeCol);

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Maps a SkeletonPoint to lie within our render space and converts to Point
        /// </summary>
        /// <param name="skelpoint">point to map</param>
        /// <returns>mapped point</returns>
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Convert point to depth space.  
            // We are not using depth directly, but we do want the points in our 640x480 output resolution.
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        /// <summary>
        /// Draws a bone line between two joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw bones from</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="jointType0">joint to start drawing from</param>
        /// <param name="jointType1">joint to end drawing at</param>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0, JointType jointType1, Pen changeCol)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == JointTrackingState.NotTracked ||
                joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == JointTrackingState.Inferred &&
                joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = changeCol;   //this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, this.SkeletonPointToScreen(joint0.Position), this.SkeletonPointToScreen(joint1.Position));

        }



        void Play_Click(object sender, RoutedEventArgs e)
        {

            media_top.Play();
            media_bottom.Play();
            SystemSounds.Asterisk.Play();

        }

        void Pause_Click(object sender, RoutedEventArgs e)
        {
            if (media_top.CanPause && media_bottom.CanPause)
            {
                media_top.Pause();
                media_bottom.Pause();
                SystemSounds.Asterisk.Play();
            }
        }

        void Stop_Click(object sender, RoutedEventArgs e)
        {
            media_top.Stop();
            media_bottom.Stop();
            SystemSounds.Asterisk.Play();
        }

        void Media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            // Handle failed media event
        }

        void Media_MediaOpened(object sender, RoutedEventArgs e)
        {
            string source_video1 = "";
            string source_video2= "";
            string root_dir = AppDomain.CurrentDomain.BaseDirectory;
            if (start_exercise_flag == true)
            {
                source_video1 = root_dir + "Videos/1.mp4";
                source_video2 = root_dir + "Videos/2.mp4";
            }
            else if(start_exercise_flag == false)
            {
                source_video1 = root_dir + "Videos/instruct1.mp4";
                source_video2 = root_dir + "Videos/instruct2.mp4";
            }
           
            if(source_video1 != "" && source_video2 != "")
            {
                media_bottom.Source = new Uri(source_video1);
                media_top.Source = new Uri(source_video2);
            }
             
        }

        void Media_MediaEnded(object sender, RoutedEventArgs e)
        {   
            // The video will start over when it is done playing
            media_top.Position = TimeSpan.Zero;
            media_top.Play();
            media_bottom.Position = TimeSpan.Zero;
            media_bottom.Play();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            media_top.Stop();
            media_bottom.Stop();
            this.Hide();
            Window welcome = new Welcome();
            welcome.Show();
            SystemSounds.Asterisk.Play();
        }


        private Pen SitDownExercise(Skeleton skel)
        {
            Joint lefthip = skel.Joints[JointType.HipLeft];
            Joint righthip = skel.Joints[JointType.HipRight];
            Joint leftknee = skel.Joints[JointType.KneeLeft];
            Joint rightknee = skel.Joints[JointType.KneeRight];

            double leftAngle = GetJointAngle(leftknee, lefthip);
            double rightAngle = GetJointAngle(rightknee, righthip);


            Pen changeCol = new Pen();
            Pen green = new Pen(Brushes.Green, 6);
            Pen red = new Pen(Brushes.Red, 6);
           

            
                if (leftAngle > 10 && leftAngle < 50 && rightAngle > 130 && rightAngle < 170)
                {
                   if(time_flag)
                   {
                      if (flag == false)
                        {
                           if(reps.Count == num_count && done_flag)
                            {
                                done_flag = false;
                                rep_timer.Stop();
                                immediate_feedback.Text = "";
                                Publish_Set_Feedback();
                                scorecard_viewbox.Visibility = System.Windows.Visibility.Visible;
                                time_flag = false;
                            }
                        }
                        else if(flag==true)
                        {
                            flag = false;
                            Rep_Info rep = new Rep_Info();
                            rep.number = reps.Count;
                            rep.delta_time = rep_counter;
                            if(rep_counter < 6)
                            {
                                rep.performance = EXCELLENT;
                            }
                            else if (rep_counter < 10)
                            {
                                rep.performance = GOOD;
                            }
                            else if (rep_counter > 10)
                            {
                                rep.performance = AVERAGE;
                            }
                            Publish_Rep_Feedback(rep);
                            reps.Add(rep);
                            rep_counter = 0;
                        }
                    }
                    
                    changeCol = green;
                }
                else if (leftAngle > 57 && leftAngle < 97 && rightAngle > 83 && rightAngle < 123)
                {
                    changeCol = green;
                    flag = true;
                    if(time_flag)
                    {
                        SystemSounds.Asterisk.Play();
                    }         
                }
                else
                {
                    changeCol = red;
                }

                return changeCol;
        }

        private double GetJointAngle(Joint zeroJoint, Joint angleJoint)
        {
            Point zeroPoint = GetJointPoint(zeroJoint);
            Point anglePoint = GetJointPoint(angleJoint);
            Point x = new Point(zeroPoint.X + anglePoint.X, zeroPoint.Y);
            double a;
            double b;
            double c;
            a = Math.Sqrt(Math.Pow(zeroPoint.X - anglePoint.X, 2) +
            Math.Pow(zeroPoint.Y - anglePoint.Y, 2));
            b = anglePoint.X;
            c = Math.Sqrt(Math.Pow(anglePoint.X - x.X, 2) + Math.Pow(anglePoint.Y - x.Y, 2));
            double angleRad = Math.Acos((a * a + b * b - c * c) / (2 * a * b));
            double angleDeg = angleRad * 180 / Math.PI;
            if (zeroPoint.Y < anglePoint.Y)
            {
                angleDeg = 360 - angleDeg;
            }
            return angleDeg;
        }

        private Point GetJointPoint(Joint joint)
        {
            DepthImagePoint point = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(joint.Position,
            this.sensor.DepthStream.Format);

            point.X *= (int)this.ActualWidth / this.sensor.DepthStream.FrameWidth;
            point.Y *= (int)this.ActualHeight / this.sensor.DepthStream.FrameHeight;
            System.Console.WriteLine(point.X);
            return new Point(point.X, point.Y);
        }



        // Sonny's Code

        // Changed screenshot capture to saving text file due to "reference ambiguity" errors
        // System.Drawing and System.Window.Media.Imaging

        private void save_file_Click(object sender, EventArgs e)
        {
            SystemSounds.Asterisk.Play();

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt|All files (*.*)|*.*";

            if (saveFileDialog.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog.FileName, scorecard_textbox.Text);
            }
        }
          
        

        // Send Email

        private void send_email_Click(object sender, EventArgs e)
        {

            try
            {

                SystemSounds.Asterisk.Play();

                MailMessage mail = new MailMessage();

                // SMTP address and port

                // Gmail SMTP: smtp.gmail.com
                // Gmail port: 465

                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 465);

                // Send from (email address):

                mail.From = new MailAddress("sonnynomnom@gmail.com");

                // Send to (email address):

                mail.To.Add("li.989@buckeyemail.osu.edu");

                mail.Subject = "NeuKinetics";

                StringBuilder sbBody = new StringBuilder();

                sbBody.AppendLine("Performance Analysis");

                sbBody.AppendLine("Patient ID: 00065");

                mail.Body = sbBody.ToString();

                // Text file path

                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(@"C:\Users\art\Desktop\test\score.txt");

                mail.Attachments.Add(attachment);

                // Username (Gmail) and password

                SmtpServer.Credentials = new System.Net.NetworkCredential("sonnynomnom@gmail.com", "leehorse76421!");

                SmtpServer.Port = 25;
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("Performance data has been submitted to the NeuKinetics mailbox.");

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }


        // Start Exercise Button Click

        private void start_exercise_btn_Click(object sender, RoutedEventArgs e)
        {
            media_top.Stop();
            media_bottom.Stop();
            media_bottom.Visibility = System.Windows.Visibility.Hidden;
            media_top.Visibility = System.Windows.Visibility.Hidden;
            countdown = new DispatcherTimer();
            countdown.Start();
            start_exercise_btn.Visibility = System.Windows.Visibility.Hidden;
            countdown.Interval = TimeSpan.FromSeconds(1); 
            countdown.Tick += countdown_Tick;
            SystemSounds.Asterisk.Play();
            start_exercise_flag = true;
            
        }

        public void countdown_Tick(object sender, EventArgs e) 
        {
            counter = counter - 1;
            if (counter == 4)
            {
                instruction_textbox.Text = "READY";
            }
            else 
            {
                instruction_textbox.Text = counter + "";
                

                if (counter == 0)
                {
                    instruction_textbox.Text = "BEGIN";
                    string source_video1 = "";
                    string source_video2 = "";
                    string root_dir = AppDomain.CurrentDomain.BaseDirectory;
                    if (start_exercise_flag == true)
                    {
                        source_video1 = root_dir + "Videos/1.mp4";
                        source_video2 = root_dir + "Videos/2.mp4";
                    }

                    if (source_video1 != "" && source_video2 != "")
                    {
                        media_bottom.Source = new Uri(source_video1);
                        media_top.Source = new Uri(source_video2);
                    }
                    media_top.Visibility = System.Windows.Visibility.Visible;
                    media_bottom.Visibility = System.Windows.Visibility.Visible;
                    media_bottom.Volume = 0;
                    media_top.Volume = 0;
                    media_top.Play();
                    media_bottom.Play();
                }

                if (counter == -1)
                {
                    instruction_textbox.Visibility = System.Windows.Visibility.Hidden;
                    countdown.Stop();
                    time_flag = true;
                    rep_timer  = new DispatcherTimer();
                    rep_timer.Start();
                    rep_timer.Interval = TimeSpan.FromSeconds(1);
                    rep_timer.Tick += rep_timer_Tick;
                }
                SystemSounds.Beep.Play(); // sound
            }
        }


        public void rep_timer_Tick(object sender, EventArgs e) 
        {
            rep_counter++;
        }

        /* 
         * Accepts the data associated with a rep
         * and appends a summary to the feedback
         * box.
         */
        private void Publish_Rep_Feedback(Rep_Info rep)
        {
            // Set the rep label
            string repLabel = "\n" + '#' + (rep.number + 1).ToString();

            // Determine the performance label
            string performanceLabel = performance_labels[rep.performance];

            // Determine the time label
            string timeLabel = '(' + (rep.delta_time).ToString() + "s.)";

            string message = repLabel + ": " + performanceLabel + ' ' + timeLabel;
            scorecard_textbox.Text += message + '\n';

            immediate_feedback.Text = "Repetition "+(rep.number + 1)+" "+performance_labels[rep.performance]+" Time: " + rep.delta_time+" sec";
        }



        /*
         * Peforms analysis of the 'reps' array
         * and append a string to the feedback
         * box summarizing the set of reps.
         * Requires the length of reps > 0 
        */

        public void Publish_Set_Feedback()
        {
            // Calculate the total set duration

            int duration = 0;
            int performance = 0;
            for (int i = 0; i < reps.Count; i++)
            {
                duration += reps[i].delta_time;
                performance += reps[i].performance;
            }
            
            if(reps.Count>0)
            {
                performance = (performance / reps.Count); // average the performance
            }
            

            scorecard_textbox.Text += '\n';
            scorecard_textbox.Text += "The set was completed in " + (duration).ToString() + " seconds.\n";
            scorecard_textbox.Text += "Overall, performance was " + performance_labels[performance] + '\n';

        }



        private void scorecard_btn_Click(object sender, RoutedEventArgs e)
        {
            if (scorecard_viewbox.Visibility != System.Windows.Visibility.Hidden)
            {
                SystemSounds.Asterisk.Play();
                scorecard_viewbox.Visibility = System.Windows.Visibility.Hidden;
                instructions.Visibility = System.Windows.Visibility.Visible;            
            }
            else
            {
                SystemSounds.Asterisk.Play();
                instructions.Visibility = System.Windows.Visibility.Hidden;
                scorecard_viewbox.Visibility = System.Windows.Visibility.Visible;
            }
        }

        

    }


}